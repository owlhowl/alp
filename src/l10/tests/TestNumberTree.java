package l10.tests;

import net.datastructures.*;

import java.util.Arrays;

public class TestNumberTree {

	public static void main(String[] args) {
		// Define&Create: 
		LinkedBinaryTree<Integer> fBTree = new LinkedBinaryTree<Integer>();
		// Feed
		Position<Integer> fRoot = fBTree.addRoot(50);
		// First level
		Position<Integer>[] firstLevel = new Position[2];
		firstLevel[0] = fBTree.addLeft(fRoot, 25);
		firstLevel[1] = fBTree.addRight(fRoot, 75);
		// Second Level
		Position<Integer>[] secondLevel = new Position[4];
		secondLevel[0] = fBTree.addLeft(firstLevel[0], 5);
		secondLevel[1] = fBTree.addRight(firstLevel[0], 35);
		secondLevel[2] = fBTree.addLeft(firstLevel[1], 60);
		secondLevel[3] = fBTree.addRight(firstLevel[1], 90);
		// Third Level
		Position<Integer>[] thirdLevel = new Position[2];
		thirdLevel[0] = fBTree.addLeft(secondLevel[0], 1);
		thirdLevel[1] = fBTree.addRight(secondLevel[0], 10);
		
		System.out.println("Height of " + fRoot.getElement() + " is: " + fBTree.height(fRoot));
		System.out.println("Height of " + firstLevel[0].getElement() + " is: " + fBTree.height(firstLevel[0]));
		System.out.println("Height of " + thirdLevel[1].getElement() + " is: " + fBTree.height(thirdLevel[1]));
		//
		System.out.println(TreeUtils.toStringIndentTree(fBTree));
		System.out.println("-------------------------------------------------------------");
		
		// Manipulate&Process: traversal
		System.out.println("fBTree.preorder : " 	+ TreeUtils.toStringTreePositions(fBTree.preorder()));

		System.out.println("-------------------------------------------------------------");
		Position<Integer> searchedPosition = searchBTreePreOrder(fBTree, 35);
		if(searchedPosition != null)
			System.out.println("Search result: " + searchedPosition.getElement());
		else
			System.out.println("Not found!");
		System.out.println("fBTree.inorder : " 		+ TreeUtils.toStringTreePositions(fBTree.inorder()));
		System.out.println("In Order:");
		printInorder(fBTree.root(), fBTree);
		System.out.println();
		System.out.println("Post Order:");
		printPostorder(fBTree.root(), fBTree);
		System.out.println();
		System.out.println("Pre Order:");
		printPreorder(fBTree.root(), fBTree);
		System.out.println();
		System.out.println("BFS:");
		printBFS(fBTree.root(), fBTree);
	}
	
	public static <E> Position<E> searchBTreePreOrder(LinkedBinaryTree<E> BTree, E searchElement){
		return searchNode(BTree, BTree.root(), searchElement);
	}
	
	private static <E> Position<E> searchNode(LinkedBinaryTree<E> BTree, Position<E> node, E searchElement){
		if (node.getElement().equals(searchElement)) {
			return node;
		}
		
//		if (BTree.isExternal(node)) {
//			return null;
//		}
		
		Comparable<E> comparableSearchElement = (Comparable<E>) searchElement;
		if (comparableSearchElement.compareTo(node.getElement()) < 0) {
			return searchNode(BTree, BTree.left(node), searchElement);
		} else {
			return searchNode(BTree, BTree.right(node), searchElement);
		}
	}

	static void printInorder(Position<Integer> node, LinkedBinaryTree<Integer> BTree)
	{
		if (node == null) {
			return;
		}

		/* first recur on left child */
		printInorder(BTree.left(node), BTree);

		/* then print the data of node */
		System.out.print(node.getElement() + " ");

		/* now recur on right child */
		printInorder(BTree.right(node), BTree);
	}

	static void printPreorder(Position<Integer> node, LinkedBinaryTree<Integer> BTree)
	{
		if (node == null) {
			return;
		}

		System.out.print(node.getElement() + " ");
		printPreorder(BTree.left(node), BTree);
		printPreorder(BTree.right(node), BTree);
	}

	static void printPostorder(Position<Integer> node, LinkedBinaryTree<Integer> BTree)
	{
		if (node == null) {
			return;
		}

		printPostorder(BTree.left(node), BTree);
		printPostorder(BTree.right(node), BTree);
		System.out.print(node.getElement() + " ");
	}

	static void printBFS(Position<Integer> node, LinkedBinaryTree<Integer> BTree) {
		Queue<Position<Integer>> queue = new LinkedQueue<>();
		queue.enqueue(node);

		while (!queue.isEmpty()) {
			Position<Integer> temp = queue.dequeue();
			System.out.print(temp.getElement() + " ");

			if (BTree.left(temp) != null) {
				queue.enqueue(BTree.left(temp));
			}

			if (BTree.right(temp) != null) {
				queue.enqueue(BTree.right(temp));
			}
		}
	}
}