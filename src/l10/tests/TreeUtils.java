package l10.tests;

import net.datastructures.AbstractTree;
import net.datastructures.Position;

public class TreeUtils {
	public static <E> String toStringTreePositions(Iterable<Position<E>> positions) {
		String treeString = "tree.positions : [";
		for (Position<E> p : positions)
			treeString += p.getElement() + ",";
		treeString += "]";
		return treeString.replace(",]", "]");
	}

	public static <E> String toStringIndentTree(AbstractTree<E> tree) {
		StringBuilder sb = new StringBuilder();
		String indentedElement = "";
		for (Position<E> p : tree.preorder()) {
			int spaceCount = 4 * (1 + tree.depth(p));
			indentedElement = String.format("%1$" + spaceCount + "s", "[" + p.getElement() + "]");
			indentedElement += "\n";
			sb.append(indentedElement);
		}
		return sb.toString();
	}
}
