package l9.bc;

import java.util.Comparator;

import net.datastructures.ArrayStack;
import net.datastructures.PriorityQueue;
import net.datastructures.SortedPriorityQueue;
import net.datastructures.Stack;
import net.datastructures.UnsortedPriorityQueue;

public class WarehousePQueue<E> {
	// WarehousEntry
	public static class WarehouseEntry<E>{
		private E entry;
		private Double amount;
		public E getEntry() {
			return entry;
		}
		public Double getAmount() {
			return amount;
		}
		public WarehouseEntry(E entry, Double amount) {
			super();
			this.entry = entry;
			this.amount = amount;
		}
		@Override
		public String toString() {
			return "[WEntry=" + entry + ", amount=" + amount + "]";
		}
	}
	private static class ComparatorWarehouseAmount implements Comparator<Double>{
		@Override
		public int compare(Double o1, Double o2) {
			return o1.compareTo(o2);
		}
	}
	//
	private Double threshold = 0.0;
	private Double fillingLevel = 0.0;
	private Double warehouseMaxCapacity = 0.0;
	
	// Use ComparatorWarehouseAmount to release largest items(amounts) firstly
	private PriorityQueue<Double, WarehouseEntry<E>> wQueue = 
			new UnsortedPriorityQueue<>(new ComparatorWarehouseAmount()); 
			//new SortedPriorityQueue<>(new ComparatorWarehouseAmount());
			
	//
	public WarehousePQueue(Double threshold, Double warehouseMaxCapacity) {
		super();
		this.threshold = threshold;
		this.warehouseMaxCapacity = warehouseMaxCapacity;
	}

	public Stack<E> addIntoWarehouse(E entry, Double amount){
		System.out.println("... adding:" + entry + " with: " + amount);
		
		// check if there is enough room even after releasing some older items
		if (this.fillingLevel + amount > this.warehouseMaxCapacity &&
				this.threshold + amount > this.warehouseMaxCapacity) 
			throw new RuntimeException("Not enough space in warehouse!");
		// firstly remove some items to make enough room for the new entry
		Stack<E> releasedItems = releaseItemsFromWarehouse();
		
		WarehouseEntry<E> newEntry = new WarehouseEntry<E>(entry, amount);
		// add the new entry
		this.wQueue.insert(newEntry.getAmount(), newEntry);
		// increase warehouse filling level
		this.fillingLevel += newEntry.getAmount();
		return releasedItems;
	}
	
	private Stack<E> releaseItemsFromWarehouse() {
		Stack<E> releasedItems = new ArrayStack<E>();
		WarehouseEntry<E> releasedEntry;
		// release item until warehouse filling level fall under warehouse threshold
		while(this.fillingLevel > this.threshold) {
			// release item under FIFO rule
			releasedEntry = this.wQueue.removeMin().getValue();
			// increase warehouse filling level
			this.fillingLevel -= releasedEntry.getAmount();
			releasedItems.push(releasedEntry.getEntry());
		}
		return releasedItems;
	}

	@Override
	public String toString() {
		return "WarehousePQueue [threshold=" + threshold + ", fillingLevel=" + fillingLevel + ", warehouseMaxCapacity="
				+ warehouseMaxCapacity + ", wQueue.size=" + wQueue.size() + "]";
	}

	/*
	 * 
	 */
	public static void main(String[] args) {
		WarehousePQueue<String> warehouseQueue = new WarehousePQueue<String>(60.0, 100.0);
		Stack<String> releasedItems;
		//
		releasedItems = warehouseQueue.addIntoWarehouse("Item_1", 20.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
		
		releasedItems = warehouseQueue.addIntoWarehouse("Item_2", 40.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
		
		releasedItems = warehouseQueue.addIntoWarehouse("Item_3", 30.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
		
		releasedItems = warehouseQueue.addIntoWarehouse("Item_4", 20.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
		
		releasedItems = warehouseQueue.addIntoWarehouse("Item_5", 15.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
		
		releasedItems = warehouseQueue.addIntoWarehouse("Item_6", 10.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
		
		releasedItems = warehouseQueue.addIntoWarehouse("Item_7", 40.0);
		System.out.println(">>> Warehouse: " + warehouseQueue);
		System.out.println(">>> >>> releasedItems: " + releasedItems);
		System.out.println("-------------------------------------------");
	}
	
}
