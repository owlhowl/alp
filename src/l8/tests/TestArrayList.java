package l8.tests;

import java.util.Iterator;

import l8.bc.ArrayListScoreboard;

import dsaj.arrays.GameEntry;
import net.datastructures.ArrayList;
import net.datastructures.List;

public class TestArrayList {

	public static void main(String[] args) {
		testArrayListOps();
		testArrayListScoreboard();
	}
	
	public static void testArrayListOps() {
		// Define&Create
		List<String> aList = new ArrayList<String>(5); 
		
		
		// Feed
		for (int i = 0; i < 10; i++) {
			aList.add(i, "Element_" + i);
		}
		System.out.println("1 ########################");
		System.out.println("aList: " + aList);
		System.out.println("##########################\n");
		
		
		// Manipulate
		aList.remove(2);
		aList.remove(4);
		aList.set(5, "New_Element_5");
		System.out.println("2 ########################");
		System.out.println("aList: " + aList);
		System.out.println("##########################\n");
		
		
		// Process
		String element = aList.get(5);
		System.out.println("3 ########################");
		System.out.println("aList.get(5) = " + element);
		System.out.println("##########################\n");
		
		
//		for (int i = 0; i < 10; i++)
//			System.out.println("Element pozitia " + i + ": " + aList.get(i));
		
		
	
		System.out.println("4 ########################");
		Iterator<String> iterator = aList.iterator();
		while(iterator.hasNext()) {
			System.out.println("aList next element: " + iterator.next());
		}
		System.out.println("##########################\n");
		
		
		System.out.println("5 ########################");
		for(String e: aList) {
			System.out.println("aList element: " + e);
		}
		System.out.println("##########################\n");
		
	}
	
	public static void testArrayListScoreboard() {
		// The main method
		ArrayListScoreboard highscores = new ArrayListScoreboard(5);
		String[] names = { "Rob", "Mike", "Rose", "Jill", "Jack", "Anna", "Paul", "Bob" };
		int[] scores = { 750, 1105, 590, 740, 510, 660, 720, 400 };

		System.out.println("6 ########################");
		for (int i = 0; i < names.length; i++) {
			GameEntry gE = new GameEntry(names[i], scores[i]);
			System.out.println("Adding " + gE);
			highscores.add(gE);
			System.out.println(" Scoreboard: " + highscores);
		}
		System.out.println("##########################\n");
		
		
		System.out.println("7 ########################");
		System.out.println("Removing score at index " + 3);
		highscores.remove(3);
		System.out.println(highscores);
		System.out.println("##########################\n");
		
		
		System.out.println("8 ########################");
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);
		System.out.println("##########################\n");
		
		
		System.out.println("9 ########################");
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		System.out.println("##########################\n");
		
		
		System.out.println("10 ########################");
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		System.out.println("##########################\n");
		
		
		System.out.println("11 ########################");
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);
		System.out.println("##########################\n");
	}
}