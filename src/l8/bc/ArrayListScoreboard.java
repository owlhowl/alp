package l8.bc;

import dsaj.arrays.GameEntry;
import net.datastructures.ArrayList;
import net.datastructures.List;

/** Class for storing high scores in an array in nondecreasing order. */
public class ArrayListScoreboard {
	// private int numEntries = 0; // number of actual entries
	// private GameEntry[] board; // array of game entries (names & scores)
	private List<GameEntry> board;
	private int capacity;
	/**
	 * Constructs an empty scoreboard with the given capacity for storing entries.
	 */
	public ArrayListScoreboard(int capacity) {
		// board = new GameEntry[capacity];
		board = new ArrayList<GameEntry>(capacity);
		this.capacity = capacity;
	}

	/** Attempt to add a new score to the collection (if it is high enough) */
	public void add(GameEntry e) {
		// board will add its first element (is empty)
		if (board.isEmpty()) {
			board.add(0, e);
			return;
		}
				
		int newScore = e.getScore();
		// is the new entry e really a high score?
		if (this.board.size() < this.capacity || newScore > board.get(this.board.size() - 1).getScore()) {
			int j = this.board.size() - 1;
			while (j > 0 && board.get(j - 1).getScore() < newScore) {
				board.add(j, board.get(j - 1));
				j--; // and decrement j
			}
			board.add(j, e);
		}
	}

	/** Remove and return the high score at index i. */
	public GameEntry remove(int i) throws IndexOutOfBoundsException {
		if (i < 0 || i >= this.board.size())
			throw new IndexOutOfBoundsException("Invalid index: " + i);
		
		GameEntry temp = board.get(i);
		board.remove(i);
		return temp; // return the removed object
	}

	/** Returns a string representation of the high scores list. */
	public String toString() {
		StringBuilder sb = new StringBuilder("[");
		sb.append(this.board);
		sb.append("]");
		return sb.toString();
	}

	public static void main(String[] args) {
		// The main method
		ArrayListScoreboard highscores = new ArrayListScoreboard(5);
		String[] names = { "Rob", "Mike", "Rose", "Jill", "Jack", "Anna", "Paul", "Bob" };
		int[] scores = { 750, 1105, 590, 740, 510, 660, 720, 400 };

		for (int i = 0; i < names.length; i++) {
			GameEntry gE = new GameEntry(names[i], scores[i]);
			System.out.println("Adding " + gE);
			highscores.add(gE);
			System.out.println(" Scoreboard: " + highscores);
		}
		System.out.println("Removing score at index " + 3);
		highscores.remove(3);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);
	}
}
