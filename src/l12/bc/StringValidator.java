package l12.bc;

public class StringValidator {
	//
	public static void main(String[] args) {
		String name = "FirstName SecondName";
		System.out.println("Validate name: " +  name + " :" + validateFullName(name));
		name = "Firstname_Secondname";
		System.out.println("Validate name: " +  name + " :" + validateFullName(name));
		System.out.println("--------------------------------------------------------");
		
		String decimalNumber = "123.12";
		System.out.println("Validate decimal: " +  decimalNumber + " :" 
				+ validateNumberWithDecimalPositions(decimalNumber, 3));
		decimalNumber = "123.123";
		System.out.println("Validate decimal: " +  decimalNumber + " :" 
				+ validateNumberWithDecimalPositions(decimalNumber, 3));
		System.out.println("--------------------------------------------------------");
		
		String dataCalendaristica = "12-05-2019";
		System.out.println("Validate dataCalendaristica: " +  dataCalendaristica + " :" 
				+ validateDateEET(dataCalendaristica));
		dataCalendaristica = "12/05/2019";
		System.out.println("Validate dataCalendaristica: " +  dataCalendaristica + " :" 
				+ validateDateEET(dataCalendaristica));
		System.out.println("--------------------------------------------------------");
		
		String email = "nume@domeniu.org";
		System.out.println("Validate email: " +  email + " :" + validateEmail(email));
		email = "nume.at.domeniu.org";
		System.out.println("Validate email: " +  email + " :" + validateEmail(email));
		System.out.println("--------------------------------------------------------");
		
		String url = "https://www.openjdk.java.net";
		System.out.println("Validate url: " +  url + " :" + validateURL(url));
		url = "https://openjdk";
		System.out.println("Validate url: " +  url + " :" + validateURL(url));
		System.out.println("--------------------------------------------------------");
		
	}

	/*
	 * ^[A-Za-z]+ la inceput caractere alfabetice
	 * \\s apoi spatiu ca delimitator
	 * [A-Za-z]+$ urmeaza mai multe caractere alfabetice la sfarsit  
	 */
	static boolean validateFullName(String fullName) {
		String titlePattern = "^([A-Za-z]+(\\s))+[A-Za-z]+$";
		return fullName.matches(titlePattern);
	}
	
	/*
	 * \\d+ caractere numerice  \d cel putin 1
	 * \\. separator zecimal punct
	 * \\d{2} urmeaza alte caractere numerice exact 2 pozitii
	 */
	static boolean validateNumberWithDecimalPositions(String numericString, Integer decimalPositions) {
		String numericPattern = "\\d+(\\.\\d{" + decimalPositions + "})?";
		return numericString.matches(numericPattern);
	}
	
	/*
	 * ^\\d{2}: la inceptut ^ 2 pozitii numerice \d - pentru zi
	 * apoi delimitator
	 * \\d{2}: urmeaza 2 pozitii numerice - pentru luna
	 * apoi delimitatorul, de ex caracterul /
	 * \\d{4}$: 4 pozitii numerice la sfarsit $ - pentru an
	 */
	static boolean validateDateEET(String dateString) {
		String dateDelimiter = "/";
		String datePattern = "^\\d{2}" 
				+ dateDelimiter + "\\d{2}" 
				+ dateDelimiter + "\\d{4}$";
		return dateString.matches(datePattern);
	}
	
	/*
	 * ^(.+) la inceput ^ orice caracter cel putin 1
	 * @ separator
	 * (.+)$ orice caracter cel putin 1 la sfarsit $
	 */
	static boolean validateEmail(String emailString) {
		String emailPattern = "^(.+)@(.+)$";
		return emailString.matches(emailPattern);
	}
	
	/*
	 * ^(http://|https://): la inceput http:// sau https://
	 * (www.)?: www. ar putea fi parte din URL (apare o singura data {0,1})
	 * ([a-zA-Z0-9]*(\\.)){1,3}: o secventa de caractere alfabetice [a-zA-Z0-9] 
	 * 		urmate de punct (\\.)
	 * 		cel putin 1 data, cel mult de 3 ori {1,3}
	 * oricare alte caractere de mai multe ori
	 */
	static boolean validateURL(String URLString) {
		String URLPattern = "^(http://|https://)?(www.)?([a-zA-Z0-9]*(\\.)){1,3}.*";
		return URLString.matches(URLPattern);
	}
}




// String URLPattern = "^(http://|https://)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$";
// https://howtodoinjava.com/regex/java-regex-validate-email-address/
// String URLPattern = "^(http:\\/\\/|https:\\/\\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$"
// String numericPattern = "\\d+(\\.\\d{2})?";
// String datePattern = "^\\d{4}-\\d{2}-\\d{2}$";
// String datePattern = "^\\d{2}-\\d{2}-\\d{4}$";
