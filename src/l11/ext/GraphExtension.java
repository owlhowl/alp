package l11.ext;


import l11.tests.GraphUtils;

import net.datastructures.AdjacencyMapGraph;
import net.datastructures.Edge;
import net.datastructures.Graph;
import net.datastructures.LinkedPositionalList;
import net.datastructures.Map;
import net.datastructures.PositionalList;
import net.datastructures.UnsortedTableMap;
import net.datastructures.Vertex;

// Graf cu elemente-in-varfuri din valori String si ponderi-in-muchii din valori Integer
// Elementele (etichetele) String nu se pot repeta in graf: nu pot exista 2 varfuri cu aceeasi eticheta
public class GraphExtension extends AdjacencyMapGraph<String, Integer>{
	
	public static void main(String[] args) {
		String[][] charEdges = {
			      {"A","B"}, {"A","E"}, {"A","F"}, 
			      {"B","C"}, {"B","F"},
			      {"C","D"}, {"C","G"}, 
			      {"D","G"}, {"D","H"}, 
			      {"E","F"}, {"E","I"}, 
			      {"F","I"}, 
			      {"G","J"}, {"G","K"}, {"G","L"},
			      {"H","L"}, 
			      {"I","J"}, {"I","M"}, {"I","N"}, 
			      {"J","K"},
			      {"K","N"}, {"K","O"}, 
			      {"L","P"}, 
			      {"M","N"},
			    };
		GraphExtension gChars = new GraphExtension(true);
		for (String[] edge: charEdges)
			gChars.addEdge(edge);
		
		System.out.println(gChars);
		System.out.println("------------------------------------------");
		
		System.out.println(">>> DFS");
		Map<Vertex<String>, Integer> known = new UnsortedTableMap<>();
		Map<Vertex<String>, Edge<Integer>> forest = new UnsortedTableMap<>();
		DFS(gChars, gChars.getVertex("A"), known, forest);
		for(Vertex<String> v: known.keySet())
			System.out.print(v.getElement() + "(" + known.get(v) + "), ");
		System.out.println();
		for(Vertex<String> v: forest.keySet())
			System.out.print(GraphUtils.toString(v, forest.get(v), gChars)+ ", ");
		System.out.println("\n------------------------------------------");
		
		System.out.println(">>> BFS");
		known = new UnsortedTableMap<>();
		forest = new UnsortedTableMap<>();
		BFS(gChars, gChars.getVertex("A"), known, forest);
		for(Vertex<String> v: known.keySet())
			System.out.print(v.getElement() + "(" + known.get(v) + "), ");
		System.out.println();
		for(Vertex<String> v: forest.keySet())
			System.out.print(GraphUtils.toString(v, forest.get(v), gChars)+ ", ");
		System.out.println("\n------------------------------------------");
		
		System.out.println(">>> DFSE");
		known = new UnsortedTableMap<>();
		Map<Vertex<String>, PositionalList<Edge<Integer>>> forestWithPath = new UnsortedTableMap<>();
		DFSE(	gChars, 
				gChars.getVertex("A"), 
				known, 
				forestWithPath
				);
		for(Vertex<String> v: known.keySet())
			System.out.print(v.getElement() + "(" + known.get(v) + "), ");
		System.out.println();
		for(Vertex<String> v: forestWithPath.keySet()) {
			System.out.print(GraphUtils.toStringAll(v, forestWithPath.get(v), gChars) + ", ");
		}
		System.out.println("\n------------------------------------------");
		
		System.out.println(">>> BFSE");
		known = new UnsortedTableMap<>();
		forestWithPath = new UnsortedTableMap<>();
		BFSE(	gChars, 
				gChars.getVertex("A"), 
				known, 
				forestWithPath
				);
		for(Vertex<String> v: known.keySet())
			System.out.print(v.getElement() + "(" + known.get(v) + "), ");
		System.out.println();
		for(Vertex<String> v: forestWithPath.keySet()) {
			System.out.print(GraphUtils.toStringAll(v, forestWithPath.get(v), gChars) + ", ");
		}
		System.out.println("\n------------------------------------------");
	}
	
	// cauta Vertex dupa eticheta
	public Vertex<String> getVertex(String vertexLabel){
		for(Vertex<String> v: vertices())
			if (v.getElement().equals(vertexLabel))
				return v;
		return null;
	}
	// Creaza subgraf din graf pornind de la o lista de muchii
	//
	
	// Depth-First Search
	// known  - tabela pentru inventariere varfuri si de cate ori au fost atinse
	// forest - tabela pentru inventariere varfuri si muchia prin care au fost atinse
	public static void DFS(
			Graph<String, Integer> g, Vertex<String> u, 
			Map<Vertex<String>, Integer> known, 
			Map<Vertex<String>, Edge<Integer>> forest) {
		// marcheaza v ca fiind descoperit
		known.put(u, 1); // u has been discovered
		// for e in [muchiile externe de forma e=(u,v)]
		for (Edge<Integer> e : g.outgoingEdges(u)) { // for every outgoing edge from u
			Vertex<String> v = g.opposite(u, e);
			// daca v nu a fost parcurs
			if (known.get(v)==null) {
				// retine muchia e ca muchie de descoperire pentru varful v
				forest.put(v, e); // e is the tree edge that discovered v
				// apel recursiv DFS pentru v
				DFS(g, v, known, forest); // recursively explore from v
			}else
				known.put(v, known.get(v)+1 ); // how many times v was reached?
		}
	}
	
	// Breadth-First Search
	// known  - tabela pentru inventariere varfuri si de cate ori au fost atinse
	// forest - tabela pentru inventariere varfuri si muchia prin care au fost atinse	
	public static void BFS(
			Graph<String, Integer> g, Vertex<String> s, 
			Map<Vertex<String>, Integer> known, 
			Map<Vertex<String>, Edge<Integer>> forest) {
		PositionalList<Vertex<String>> level = new LinkedPositionalList<>();
		known.put(s, 1);
		level.addLast(s); // first level includes only s
		while (!level.isEmpty()) {
			PositionalList<Vertex<String>> nextLevel = new LinkedPositionalList<>();
			for (Vertex<String> u : level)
				for (Edge<Integer> e : g.outgoingEdges(u)) {
					Vertex<String> v = g.opposite(u, e);
					if (known.get(v)==null) {
						known.put(v, known.get(v) == null? 1 : known.get(v)+1 );
						forest.put(v, e); // e este calea de descoperire a varfului v
						nextLevel.addLast(v); // v va fi luat in considerare in urmatorul pas
					}else
						known.put(v, known.get(v)+1 ); // v a fost inca o data atins
				}
			level = nextLevel; // nivelul next va deveni nivelul curent de procesare
		}
	}
	
	// Depth-First Search
	// known  - tabela pentru inventariere varfuri si de cate ori au fost atinse
	// forest - tabela pentru inventariere varfuri si calea PositionalList 
	// 			formata din muchiile prin care au fost atinse
	public static void DFSE(
			Graph<String, Integer> g, Vertex<String> u, 
			Map<Vertex<String>, Integer> known, 
			Map<Vertex<String>, PositionalList<Edge<Integer>>> forest 
			) {
		// marcheaza v ca fiind descoperit
		known.put(u, 1);
		// for e in [muchiile externe de forma e=(u,v)]
		for (Edge<Integer> e : g.outgoingEdges(u)) { 
			Vertex<String> v = g.opposite(u, e);
			// daca v nu a fost parcurs
			if (known.get(v)==null) {
				// creaza calea de muchii pentru nodul v 
				PositionalList<Edge<Integer>> newEdgePath = new LinkedPositionalList<Edge<Integer>>();
				// preluarea caii de access a nodului precedent
				if (forest.get(g.opposite(v, e)) != null)
					for (Edge<Integer> edge: forest.get(g.opposite(v, e))) newEdgePath.addLast(edge);
				// adaugare in cale a muchiei curente prin care a fost descoperit v
				newEdgePath.addLast(e); // e is the tree edge that discovered v
				//
				forest.put(v, newEdgePath); 
				// apel recursiv DFS pentru v
				DFSE(g, v, known, forest); // recursively explore from v
			}else {
				known.put(v, known.get(v)+1 ); // how many times v was reached?
			}
		}
	}
	
	// Breadth-First Search Enhanced
	// known  - tabela pentru inventariere varfuri si de cate ori au fost atinse
	// forest - tabela pentru inventariere varfuri si calea PositionalList 
	// 			formata din muchiile prin care au fost atinse
	public static void BFSE(
			Graph<String, Integer> g, Vertex<String> s, 
			Map<Vertex<String>, Integer> known, 
			Map<Vertex<String>, PositionalList<Edge<Integer>>> forest 
			) {
		PositionalList<Vertex<String>> level = new LinkedPositionalList<>();
		known.put(s, 1);
		level.addLast(s); // first level includes only s
		while (!level.isEmpty()) {
			PositionalList<Vertex<String>> nextLevel = new LinkedPositionalList<>();
			for (Vertex<String> u : level)
				for (Edge<Integer> e : g.outgoingEdges(u)) {
					Vertex<String> v = g.opposite(u, e);
					if (known.get(v)==null) {
						known.put(v, known.get(v) == null? 1 : known.get(v)+1 );
						// creaza calea de muchii pentru nodul v 
						PositionalList<Edge<Integer>> newEdgePath = new LinkedPositionalList<Edge<Integer>>();
						// preluarea caii de access a nodului precedent
						if (forest.get(g.opposite(v, e)) != null)
							for (Edge<Integer> edge: forest.get(g.opposite(v, e))) newEdgePath.addLast(edge);
						// adaugare in cale a muchiei curente
						newEdgePath.addLast(e);
						//
						forest.put(v, newEdgePath); // e is the tree edge that discovered v
						//
						nextLevel.addLast(v); // v will be further considered in next pass
					}else
						known.put(v, known.get(v)+1 ); // how many times v was reached
				}
			level = nextLevel; // relabel 'next' level to become the current
		}
	} //
	
	public GraphExtension(boolean directed) {
		super(directed);
	}
	
	// Creare muchie graf din Array
	void addEdge(String[] edgeArray) {
		// caut varf sursa
		Vertex<String> u = getVertex(edgeArray[0]);
		// daca nu a fost deja introdus va fi inserat
		if (u == null)
			u = insertVertex(edgeArray[0]);
		// caut varf destinatie
		Vertex<String> v = getVertex(edgeArray[1]);
		// daca nu a fost deja introdus va fi inserat
		if (v == null)
			v = insertVertex(edgeArray[1]);
		// inserare muchie
		// este necesara conversia din String in Integer pentru distanta muchiei
		if (edgeArray.length > 2)
			insertEdge(u, v, Integer.valueOf(edgeArray[2]));
		else // implicit 1
			insertEdge(u, v, 1);
	}	
}
