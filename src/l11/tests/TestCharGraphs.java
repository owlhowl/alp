package l11.tests;

import net.datastructures.AdjacencyMapGraph;
import net.datastructures.Edge;
import net.datastructures.Graph;
import net.datastructures.Map;
import net.datastructures.SortedTableMap;
import net.datastructures.Vertex;

public class TestCharGraphs {

	public static void main(String[] args) {
		// Define:
		Graph<String,Integer> gChars = new AdjacencyMapGraph<String,Integer>(false);
		
		// Feed:
		// Add&Create vertices
		Map<String, Vertex<String> > verts = new SortedTableMap<>();
		verts.put("A", gChars.insertVertex("A"));
		verts.put("B", gChars.insertVertex("B"));
		verts.put("C", gChars.insertVertex("C"));
		verts.put("D", gChars.insertVertex("D"));
		verts.put("E", gChars.insertVertex("E"));
		verts.put("F", gChars.insertVertex("F"));
		verts.put("G", gChars.insertVertex("G"));
		verts.put("H", gChars.insertVertex("H"));
		verts.put("I", gChars.insertVertex("I"));
		verts.put("J", gChars.insertVertex("J"));
		verts.put("K", gChars.insertVertex("K"));
		verts.put("L", gChars.insertVertex("L"));
		verts.put("M", gChars.insertVertex("M"));
		verts.put("N", gChars.insertVertex("N"));
		verts.put("O", gChars.insertVertex("O"));
		verts.put("P", gChars.insertVertex("P"));
		// Add&Create edges
		gChars.insertEdge(verts.get("A"), verts.get("B"), 1);
		gChars.insertEdge(verts.get("A"), verts.get("E"), 1);
		gChars.insertEdge(verts.get("A"), verts.get("F"), 1);
		gChars.insertEdge(verts.get("B"), verts.get("C"), 1);
		gChars.insertEdge(verts.get("B"), verts.get("F"), 1);
		gChars.insertEdge(verts.get("C"), verts.get("D"), 1);
		gChars.insertEdge(verts.get("C"), verts.get("G"), 1);
		gChars.insertEdge(verts.get("D"), verts.get("G"), 1);
		gChars.insertEdge(verts.get("D"), verts.get("H"), 1);
		gChars.insertEdge(verts.get("E"), verts.get("F"), 1);
		gChars.insertEdge(verts.get("E"), verts.get("I"), 1);
		gChars.insertEdge(verts.get("F"), verts.get("I"), 1);
		gChars.insertEdge(verts.get("G"), verts.get("J"), 1);
		gChars.insertEdge(verts.get("G"), verts.get("K"), 1);
		gChars.insertEdge(verts.get("G"), verts.get("L"), 1);
		gChars.insertEdge(verts.get("H"), verts.get("L"), 1);
		gChars.insertEdge(verts.get("I"), verts.get("J"), 1);
		gChars.insertEdge(verts.get("I"), verts.get("M"), 1);
		gChars.insertEdge(verts.get("I"), verts.get("N"), 1);
		gChars.insertEdge(verts.get("J"), verts.get("K"), 1);
		gChars.insertEdge(verts.get("K"), verts.get("N"), 1);
		gChars.insertEdge(verts.get("K"), verts.get("O"), 1);
		gChars.insertEdge(verts.get("L"), verts.get("P"), 1);
		gChars.insertEdge(verts.get("M"), verts.get("N"), 1);
		
		// Manipulate&Process
		System.out.println(gChars);
		System.out.println("----------------------------------------------");
		
		// Cauta varf A
		String labelVarfA = "A";
		Vertex<String> A = null;
		for(Vertex<String> v: gChars.vertices())
			if (v.getElement().equals(labelVarfA))
				A = v;
		if (A != null)
			System.out.println("Am gasit: " + A.getElement());
		
		// Cauta varf F
		String labelVarfF = "F";
		Vertex<String> F = null;
		for(Vertex<String> v: gChars.vertices())
			if (v.getElement().equals(labelVarfF))
				F = v;
		if (F != null)
			System.out.println("Am gasit: " + F.getElement());
		
		// Cauta muchie A-F
		Edge<Integer> muchiaAF = null;
		if (A != null && F != null) {
			for(Edge<Integer> e: gChars.edges()) {
				Vertex<String>[] margini = gChars.endVertices(e);
				if (margini[0].equals(A) && margini[1].equals(F)) {
					muchiaAF = e;
					break;
				}
			}
		}
		if (muchiaAF != null) {
			Vertex<String>[] margini = gChars.endVertices(muchiaAF);
			System.out.println("Am gasit muchia cu marginile: " 
					+ margini[0].getElement() + "->" + margini[1].getElement()
					+ ", ponderea: " + muchiaAF.getElement());
		}
		System.out.println("----------------------------------------------");
		
		// Manipulare: sterge muchie: eroare
		// gChars.removeEdge(muchiaAF);
		// gChars.removeVertex(F);
		// System.out.println(gChars);
	}

}
// GraphExamples
// GraphAlgorithms
// AdjacencyMapGraph

/*
String[][] charEdges = {
	      {"A","B"}, {"A","E"}, {"A","F"}, {"B","C"}, {"B","F"},
	      {"C","D"}, {"C","G"}, {"D","G"}, {"D","H"}, {"E","F"},
	      {"E","I"}, {"F","I"}, {"G","J"}, {"G","K"}, {"G","L"},
	      {"H","L"}, {"I","J"}, {"I","M"}, {"I","N"}, {"J","K"},
	      {"K","N"}, {"K","O"}, {"L","P"}, {"M","N"},};
*/